package App::Abuser v0.2.0;

=encoding utf8

=head1 NAME

App::Abuser - Perl library for locating an abuse contact

=head1 SYNOPSYS

    use App::Abuser;
    my $abuser = App::Abuser->new('log' => sub {STDERR->print(shift)});
    my $address = $abuser->search('192.0.2.0');
    if (defined $address) {
        print "An abuse e-mail addres for 192.0.2.0 is $address\n";
    } else {
        print "Not found\n";
    }

=head1 DESCRIPTION

This library retrieves WHOIS data from regional and local Internet registries.
Then it parses the data in order to find an abuse contact for an Internet
Protocol address. If the address is deemed not to be allocated, an IANA WHOIS
server is consulted first with the prospect that the address was allocated,
but this library has not yet been updated.

=head1 METHODS

=cut

use strict;
use warnings;
use open ':locale';
use List::MoreUtils ();
use Data::Dumper;
use Socket ();
use IO::Socket::IP ();
use PerlIO::encoding ();

=head2 new(I<OPTIONS>)

Create an App::Abuser object holding a configuration specified in an
I<OPTIONS> hash:

=over 4

=item B<debug> => I<CODEREF>

I<CODEREF> is a reference to a function called by the library to pass
a debugging text to the application. The function will be called with a scalar
argument which is a multi-line text, always terminated with an end-of-line.

=item B<log> => I<CODEREF>

I<CODEREF> is a reference to a function called by the library to notify the
application about a progress. The function will be called with a scalar
argument which is a multi-line text, always terminated with an end-of-line.

=back

=cut

sub new {
    my $class = shift;
    return bless {@_}, $class;
}

sub log {
    my $self = shift;
    if (defined $self->{'log'}) {
        $self->{'log'}->(shift);
    }
}

sub debug {
    my $self = shift;
    return unless defined $self->{'debug'};
    my $data = shift;
    if (ref $data eq '') {
        $self->{'debug'}->($data);
    } else {
        local $Data::Dumper::Sortkeys=1;
        $self->{'debug'}->(Data::Dumper::Dumper($data));
    }
}

# Search a RIR registry for an IP address. Return undef if not found.
# Mapping from
# <https://www.iana.org/assignments/ipv4-address-space/ipv4-address-space.txt>,
# and
# <https://www.iana.org/assignments/ipv6-unicast-address-assignments/ipv6-unicast-address-assignments.txt>.
sub address2rir {
    my $address = shift;
    my ($error, @results) = Socket::getaddrinfo($address, undef, {
            'flags' => Socket::AI_PASSIVE|Socket::AI_NUMERICHOST,
            'socktype' => Socket::SOCK_RAW
        });
    return if $error or @results != 1;

    my $tree4 =
    [
        [
            [
                [
                    [
                        [
                            [
                                [
                                    undef,              # 0/8
                                    'whois.apnic.net'   # 1/8
                                ],
                                [
                                    'whois.ripe.net',   # 2/8
                                    'whois.arin.net'    # 3/8
                                ],
                            ],
                            [
                                [
                                    'whois.arin.net',   # 4/8
                                    'whois.ripe.net'    # 5/8
                                ],
                                'whois.arin.net'    # 6/8--7/8
                            ]
                        ],
                        [
                            [
                                'whois.arin.net',   # 8/8--8/9
                                [
                                    undef,              # 10/8
                                    'whois.arin.net'    # 11/8
                                ]
                            ],
                            [
                                'whois.arin.net',   # 12/8--13/8
                                [
                                    'whois.apnic.net',  # 14/8
                                    'whois.arin.net'    # 15/8
                                ]
                            ]
                        ]
                    ],
                    [
                        'whois.arin.net',   # 16/8--23/8
                        [
                            [
                                [
                                    'whois.arin.net',   # 24/8
                                    'whois.ripe.net'    # 25/8
                                ],
                                [
                                    'whois.arin.net',   # 26/8
                                    'whois.apnic.net'   # 27/8
                                ]
                            ],
                            [
                                'whois.arin.net',   # 28/8--29/8
                                [
                                    'whois.arin.net',   # 30/8
                                    'whois.ripe.net'    # 31/8
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    [
                        [
                            'whois.arin.net',   # 32/8--35/8
                            [
                                [
                                    'whois.apnic.net',  # 36/8
                                    'whois.ripe.net'    # 37/8
                                ],
                                [
                                    'whois.arin.net',   # 38/8
                                    'whois.apnic.net'   # 39/8
                                ]
                            ]
                        ],
                        [
                            [
                                [
                                    'whois.arin.net',   # 40/8
                                    'whois.afrinic.net' # 41/8
                                ],
                                'whois.apnic.net'   # 42/8--43/8
                            ],
                            [
                                'whois.arin.net',   # 44/8--45/8
                                [
                                    'whois.ripe.net',   # 46/8
                                    'whois.arin.net'    # 47/8
                                ]
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                [
                                    'whois.arin.net',   # 48/8
                                    'whois.apnic.net'   # 49/8
                                ],
                                [
                                    'whois.arin.net',   # 50/8
                                    'whois.ripe.net'    # 51/8
                                ]
                            ],
                            [
                                [
                                    'whois.arin.net',   # 52/8
                                    'whois.ripe.net'    # 53/8
                                ],
                                'whois.arin.net'    # 54/8--55/8
                            ]
                        ],
                        [
                            [
                                [
                                    'whois.arin.net',   # 56/8
                                    'whois.ripe.net'    # 57/8
                                ],
                                'whois.apnic.net'   # 58/8--59/8
                            ],
                            [
                                'whois.apnic.net',  # 60/8--61/8
                                [
                                    'whois.ripe.net',   # 62/8
                                    'whois.arin.net'    # 63/8
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            [
                [
                    [
                        'whois.arin.net',   # 64/8--71/8
                        [
                            'whois.arin.net',   # 72/8--75/8
                            [
                                [
                                    'whois.arin.net',   # 76/8
                                    'whois.ripe.net'    # 77/8
                                ],
                                'whois.ripe.net'    # 78/8--79/8
                            ]
                        ]
                    ],
                    'whois.ripe.net'    # 80/8--95/8
                ],
                [
                    [
                        [
                            'whois.arin.net',   # 96/8--99/8
                            [
                                [
                                    'whois.arin.net',   # 100/8
                                    'whois.apnic.net'   # 101/8
                                ],
                                [
                                    'whois.afrinic.net',    # 102/8
                                    'whois.apnic.net'       # 103/8
                                ]
                            ]
                        ],
                        [
                            [
                                [
                                    'whois.arin.net',   # 104/8
                                    'whois.afrinic.net' # 105/8
                                ],
                                [
                                    'whois.apnic.net',  # 106/8
                                    'whois.arin.net'    # 107/8
                                ]
                            ],
                            [
                                [
                                    'whois.arin.net',   # 108/8
                                    'whois.ripe.net'    # 109/8
                                ],
                                'whois.apnic.net'   # 110/8--111/8
                            ]
                        ]
                    ],
                    [
                        'whois.apnic.net',  # 112/8--119/8
                        [
                            'whois.apnic.net',  # 120/8--123/8
                            [
                                'whois.apnic.net',  # 124/8--125/8
                                [
                                    'whois.apnic.net',  # 126/8
                                    undef   # 127/8
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        [
            [
                [
                    [
                        [
                            'whois.arin.net',   # 128/8--131/8
                            [
                                [
                                    'whois.arin.net',   # 132/8
                                    'whois.apnic.net'   # 133/8
                                ],
                                'whois.arin.net'    # 134/8--135/8
                            ]
                        ],
                        [
                            'whois.arin.net',   # 136/8--139/8
                            [
                                [
                                    'whois.arin.net',   # 140/8
                                    'whois.ripe.net'    # 141/8
                                ],
                                'whois.arin.net'    # 142/8--143/8
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                [
                                    'whois.arin.net',   # 144/8
                                    'whois.ripe.net'    # 145/8
                                ],
                                'whois.arin.net'    # 146/8--147/8
                            ],
                            [
                                'whois.arin.net',   # 148/8--149/8
                                [
                                    'whois.apnic.net',  # 150/8
                                    'whois.ripe.net'    # 151/8
                                ]
                            ]
                        ],
                        [
                            [
                                [
                                    'whois.arin.net',   # 152/8
                                    'whois.apnic.net'   # 153/8
                                ],
                                [
                                    'whois.afrinic.net',    # 154/8
                                    'whois.arin.net'        # 155/8
                                ]
                            ],
                            'whois.arin.net'    # 156/8--159/8
                        ]
                    ]
                ],
                [
                    [
                        [
                            [
                                'whois.arin.net',   # 160/8--161/8
                                [
                                    'whois.arin.net',   # 162/8
                                    'whois.apnic.net'   # 163/8
                                ]
                            ],
                            'whois.arin.net'    # 164/8--167/8
                        ],
                        [
                            [
                                'whois.arin.net',   # 168/8--169/8
                                [
                                    'whois.arin.net',   # 170/8
                                    'whois.apnic.net'   # 171/8
                                ]
                            ],
                            [
                                'whois.arin.net',   # 172/8--173/8
                                [
                                    'whois.arin.net',   # 174/8
                                    'whois.apnic.net'   # 175/8
                                ]
                            ]
                        ]
                    ],
                    [
                        [
                            [
                                [
                                    'whois.ripe.net',   # 176/8
                                    'whois.lacnic.net'  # 177/8
                                ],
                                [
                                    'whois.ripe.net',   # 178/8
                                    'whois.lacnic.net'  # 179/8
                                ]
                            ],
                            [
                                [
                                    'whois.apnic.net',  # 180/8
                                    'whois.lacnic.net'  # 181/8
                                ],
                                'whois.apnic.net'   # 182/8--183/8
                            ]
                        ],
                        [
                            [
                                [
                                    'whois.arin.net',   # 184/8
                                    'whois.ripe.net'    # 185/8
                                ],
                                'whois.lacnic.net'  # 186/8--187/8
                            ],
                            [
                                [
                                    'whois.ripe.net',   # 188/8
                                    'whois.lacnic.net'  # 189/8
                                ],
                                'whois.lacnic.net'  # 190/8--191/8
                            ]
                        ]
                    ]
                ]
            ],
            [
                [
                    [
                        [
                            [
                                [
                                    'whois.arin.net',   # 192/8
                                    'whois.ripe.net'    # 193/8
                                ],
                                'whois.ripe.net'    # 194/8--195/8
                            ],
                            [
                                'whois.afrinic.net',    # 196/8--197/8
                                'whois.arin.net'        # 198/8--199/8
                            ]
                        ],
                        [
                            [
                                'whois.lacnic.net', # 200/8--201/8
                                'whois.apnic.net'   # 202/8--203/8
                            ],
                            'whois.arin.net'    # 204/8--207/8
                        ]
                    ],
                    [
                        [
                            [
                                'whois.arin.net',   # 208/8--209/8
                                'whois.apnic.net'   # 210/8--211/8
                            ],
                            [
                                'whois.ripe.net',   # 212/8--213/8
                                'whois.arin.net'    # 214/8--215/8
                            ]
                        ],
                        [
                            [
                                [
                                    'whois.arin.net',   # 216/8
                                    'whois.ripe.net'    # 217/8
                                ],
                                'whois.apnic.net'   # 218/8--219/8
                            ],
                            'whois.apnic.net'   # 220/8--223/8
                        ]
                    ]
                ],
            ]
        ]
    ];

    my $tree6 = [
        [
            [
                undef,
                [
                    [
                        [
                            [
                                [
                                    [
                                        [
                                            [
                                                [
                                                    [
                                                        [
                                                            [
                                                                [
                                                                    undef, # 2000::/16
                                                                    [
                                                                        [
                                                                            [
                                                                                [
                                                                                    [
                                                                                        [
                                                                                            [
                                                                                                'whois.iana.org',   # 2001:0000::/23
                                                                                                'whois.apnic.net'   # 2001:0200::/23
                                                                                            ],
                                                                                            [
                                                                                                'whois.arin.net',   # 2001:0400::/23
                                                                                                'whois.ripe.net',   # 2001:0600::/23
                                                                                            ]
                                                                                        ],
                                                                                        [
                                                                                            'whois.ripe.net',   # 2001:0800::/22
                                                                                            'whois.apnic.net',  # 2001:0c00::/22
                                                                                        ]
                                                                                    ],
                                                                                    [
                                                                                        [
                                                                                            [
                                                                                                undef,
                                                                                                'whois.lacnic.net'  # 2001:1200::/23
                                                                                            ],
                                                                                            'whois.ripe.net'    # 2001:1400::/22
                                                                                        ],
                                                                                        [
                                                                                            [
                                                                                                'whois.arin.net',   # 2001:1800::/23
                                                                                                'whois.ripe.net'    # 2001:1a00::/23
                                                                                            ],
                                                                                            'whois.ripe.net',       # 2001:1c00::/22
                                                                                        ]
                                                                                    ]
                                                                                ],
                                                                                'whois.ripe.net'    # 2001:2000::/19
                                                                            ],
                                                                            [
                                                                                [
                                                                                    [
                                                                                        [
                                                                                            [
                                                                                                'whois.ripe.net',   # 2001:4000::/23
                                                                                                'whois.afrinic.net' # 2001:4200::/23
                                                                                            ],
                                                                                            [
                                                                                                'whois.apnic.net',  # 2001:4400::/23
                                                                                                'whois.ripe.net'    # 2001:4600::/23
                                                                                            ]
                                                                                        ],
                                                                                        [
                                                                                            [
                                                                                                'whois.arin.net',   # 2001:4800::/23
                                                                                                'whois.ripe.net'    # 2001:4a00::/23
                                                                                            ],
                                                                                            [
                                                                                                'whois.ripe.net',   # 2001:4c00::/23
                                                                                            ]
                                                                                        ]
                                                                                    ],
                                                                                    'whois.ripe.net'    # 2001:5000::/20
                                                                                ],
                                                                                undef
                                                                            ]
                                                                        ],
                                                                        [
                                                                            [
                                                                                'whois.apnic.net',  # 2001:8000::/19
                                                                                'whois.apnic.net'   # 2001:a000::/19
                                                                            ],
                                                                            undef
                                                                        ]
                                                                    ],
                                                                ],
                                                                [
                                                                    undef,  # 2002::/16 6to4 delegated to IPv4 space
                                                                    [
                                                                        [
                                                                            'whois.ripe.net',   # 2003:0000::/18
                                                                            undef
                                                                        ],
                                                                        undef
                                                                    ]
                                                                ]
                                                            ],
                                                            undef
                                                        ],
                                                        undef
                                                    ],
                                                    undef
                                                ],
                                                undef
                                            ],
                                            undef
                                        ],
                                        undef
                                    ],
                                    undef
                                ],
                                undef
                            ],
                            [
                                [
                                    [
                                        [
                                            [
                                                [
                                                    'whois.apnic.net', #2400:0000::/12
                                                    undef
                                                ],
                                                undef
                                            ],
                                            undef
                                        ],
                                        undef
                                    ],
                                    undef
                                ],
                                [
                                    [
                                        [
                                            [
                                                [
                                                    'whois.arin.net',   # 2600:0000::/12
                                                    [
                                                        [
                                                            [
                                                                [
                                                                    [
                                                                        [
                                                                            [
                                                                                [
                                                                                    [
                                                                                        [
                                                                                            [
                                                                                                'whois.arin.net',   # 2610:0000::/23
                                                                                                undef
                                                                                            ],
                                                                                            undef
                                                                                        ],
                                                                                        undef
                                                                                    ],
                                                                                    undef
                                                                                ],
                                                                                undef
                                                                            ],
                                                                            undef
                                                                        ],
                                                                        undef
                                                                    ],
                                                                    undef
                                                                ],
                                                                undef
                                                            ],
                                                            undef
                                                        ],
                                                        undef
                                                    ]
                                                ],
                                                [
                                                    [
                                                        [
                                                            [
                                                                [
                                                                    [
                                                                        [
                                                                            [
                                                                                [
                                                                                    [
                                                                                        [
                                                                                            [
                                                                                                'whois.arin.net',   # 2620:0000::/23
                                                                                                undef
                                                                                            ],
                                                                                            undef
                                                                                        ],
                                                                                        undef
                                                                                    ],
                                                                                    undef
                                                                                ],
                                                                                undef
                                                                            ],
                                                                            undef
                                                                        ],
                                                                        undef
                                                                    ],
                                                                    undef
                                                                ],
                                                                undef
                                                            ],
                                                            undef
                                                        ],
                                                        undef
                                                    ],
                                                    'whois.arin.net'    # 2630:0000::/12
                                                ]
                                            ],
                                            undef
                                        ],
                                        undef
                                    ],
                                    undef
                                ]
                            ]
                        ],
                        [
                            [
                                [
                                    [
                                        [
                                            [
                                                [
                                                    'whois.lacnic.net', # 2800:0000::/12
                                                    undef
                                                ],
                                                undef
                                            ],
                                            undef
                                        ],
                                        undef
                                    ],
                                    undef
                                ],
                                [
                                    [
                                        [
                                            [
                                                'whois.ripe.net',   # 2a00:0000::/11
                                                undef
                                            ],
                                            undef
                                        ],
                                        undef
                                    ],
                                    undef
                                ]
                            ],
                            [
                                [
                                    [
                                        [
                                            [
                                                [
                                                    'whois.afrinic.net',    # 2c00:0000::/12
                                                    undef
                                                ],
                                                undef
                                            ],
                                            undef
                                        ],
                                        undef
                                    ],
                                    undef
                                ],
                                undef
                            ]
                        ]
                    ],
                    undef
                ],
                undef
            ],
            undef
        ],
        undef
    ];

    my $tree;
    if ($results[0]->{'family'} == Socket::AF_INET) {
        $address = Socket::unpack_sockaddr_in($results[0]->{'addr'});
        $tree = $tree4;
    } elsif ($results[0]->{'family'} == Socket::AF_INET6) { 
        $address = Socket::unpack_sockaddr_in6($results[0]->{'addr'});
        $tree = $tree6;
    } else {
        return undef;
    }
    my @address = split('', unpack('B*', $address));

    for my $bit (@address) {
        $tree = $tree->[$bit];
        if (!defined $tree) {
            return ;
        }
        if (ref $tree eq '') {
            return $tree;
        }
    }
}


# Perform a WHOIS query to a server, die on en error, return standard output
# as a scalar string.
sub query {
    my ($self, $server, $query) = @_;

    if (!defined $query) {
        die sprintf("Invalid arguments for query(): %s\n", join(' ', @_));
    }

    if ($server eq 'whois.arin.net') {
        # Some network ranges have multiple records. In that case the server
        # shows only a brief list. Request full listing.
        $query = '+ ' . $query;
    } elsif ($server eq 'whois.nic.ad.jp') {
        # Request English output. Otherwise the output in Japanese in
        # iso-2022-jp charset. <https://www.nic.ad.jp/en/db/whois/index.html>
        $query .= '/e';
    }

    $self->log(sprintf("Retrieving \"%s\" from %s\n", $query, $server));
    my ($error, @results) = Socket::getaddrinfo($server, 'whois', {
            'socktype' => Socket::SOCK_STREAM
        });
    if ($error) {
        die "Could not resolve $server:whois TCP address: $error\n";
    }
    my $socket = IO::Socket::IP->new('PeerAddrInfo' => \@results) or
        die "Could not connect to a $server:whois TCP server: $@\n";
    binmode($socket, ':encoding(UTF-8)') or
        die "Could not set an encoding for a TCP socket: $!\n";

    $socket->print($query . $Socket::CRLF) or
        die "Error while sending a query to a WHOIS server: $!\n";
    $socket->flush or
        die "Error while sending a query to a WHOIS server: $!\n";
    my $output = do { local $/; <$socket>; } or
        die "Error reading from a WHOIS server: $!\n";
    $output =~ s/$Socket::CR?$Socket::LF/\n/mg;
    $socket->close or
        die "Error while closing a socket to a WHOIS server: $!\n";

    $self->debug(\$output);
    return $output;
}


# Convert a string representation of an IPv4 network range
# ("1.2.3.0 - 1.2.4.255") into a network prefix length (21).
# Return 0 in case of an error.
sub netrange2prefixlen {
    my $string = shift;

    return 0 unless $string =~ m/(\S+) - (\S+)/;
    my ($bottom, $top) = ($1, $2);
    $bottom = Socket::inet_pton(Socket::AF_INET, $bottom);
    $top = Socket::inet_pton(Socket::AF_INET, $top);
    return 0 unless defined $bottom && defined $top;
    my @bottom = split('', unpack('B*', $bottom));
    my @top = split('', unpack('B*', $top));
    return 0 unless @bottom == @top;
   
    my $offset;
    for ($offset = 0; $offset < @bottom; $offset++) {
        last if $bottom[$offset] != $top[$offset];
    }

    return $offset;
}


# Return the item, or if it is a refarray, a first item from the array. undef
# if not defined or empty.
sub first_item {
    my $variable = shift;
    if (ref $variable eq '') {
        return $variable;
    } else {
        return $variable->[0];
    }
}


# Return a reference to a hash (inetnum, role, person, organisation, route) of
# hash of objects by their identifiers.
sub parse_afrinic {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text, -1);
    my $section_name;
    my $section;
    my %objects = ('inetnum' => {}, 'role' => {}, 'organisation' => {},
        'person' => {}, 'route' => {});
    for (@lines) {
        if (m/\A\Q% [whois.apnic.net]\E/) {
            return {'redirect' => 'whois.apnic.net'};
        }
        if (m/\A\Q# ARIN WHOIS data and services\E/) {
            return {'redirect' => 'whois.arin.net'};
        }
        next if m/\A%/;
        if (m/\A\z/) {
            next unless defined $section_name;
            if ($section_name eq 'inetnum' or $section_name eq 'organisation'
                    or $section_name eq 'route' ) {
                ${$objects{$section_name}}{$section->{$section_name}} = $section;
            } elsif ($section_name eq 'role' or $section_name eq 'person') {
                ${$objects{$section_name}}{$section->{'nic-hdl'}} = $section;
            } else {
                die "Unknown AfriNIC object type $section_name.\n";
            }
            $section = undef;
            $section_name= undef;
            next;
        }
        if (m/\A([\S]+):\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if ($key eq 'nic-hdl') {
                $value = uc($value);
            }
            if (!defined $section_name) {
                $section_name = $key;
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                if ($key eq 'admin-c' or $key eq 'tech-c') {
                    $section->{$key} = [$value];
                } else { 
                    $section->{$key} = $value;
                }
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (inet6num, inetnum, irt, organisation, person,
# role, route) of hash of objects by their identifiers.
sub parse_apnic {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text);
    my $section_name;
    my $section;
    my %objects = ('inet6num' => {}, 'inetnum' => {}, 'irt' => {},
        'organisation' => {}, 'person' => {}, 'role' => {}, 'route' => {});
    for (@lines) {
        next if m/\A%/;
        if (m/\A\z/) {
            next unless defined $section_name;
            if ($section_name eq 'inet6num' or $section_name eq 'inetnum' or
                    $section_name eq 'irt' or $section_name eq 'organisation' or
                    $section_name eq 'route') {
                ${$objects{$section_name}}{$section->{$section_name}} = $section;
            } elsif ($section_name eq 'person' or $section_name eq 'role') {
                ${$objects{$section_name}}{$section->{'nic-hdl'}} = $section;
            } else {
                die "Unknown APNIC object type $section_name.\n";
            }
            $section = undef;
            $section_name= undef;
            next;
        }
        if (m/\A([\S]+):\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if ($key eq 'admin-c' or $key eq 'nic-hdl' or $key eq 'tech-c') {
                $value = uc($value);
            }
            if (!defined $section_name) {
                $section_name = $key;
            }
            if ($key eq 'inet6num' and $value =~ m{([0-9a-f:]+)/(\d+)}a) {
                # Normalize the network address. APNIC and JPNIC notations can
                # differ. We rely on the mirrored sections to mask the APNIC's
                # one, thus we need the same formats.
                $value = Socket::inet_ntop(Socket::AF_INET6,
                    Socket::inet_pton(Socket::AF_INET6, $1)) . "/" . $2;
                $section->{'prefixlen'} = $2;
            } elsif ($key eq 'inetnum') {
                $section->{'prefixlen'} = netrange2prefixlen($value);
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                if ($key eq 'admin-c' or $key eq 'tech-c' or $key eq 'remarks') {
                    $section->{$key} = [$value];
                } else { 
                    $section->{$key} = $value;
                }
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (CustName, NetRange, OrgId, OrgAbuseHandle,
# OrgDNSHandle, OrgRoutingHandle, OrgTechHandle, RAbuseHandle, ReferralServer,
# ResourceLink, RNOCHandle, RTechHandle) of hash of objects by their identifiers.
sub parse_arin {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text);
    my $disclaimer = 0;
    my $section_name;
    my $section;
    my %objects = ('CustName' => {}, 'NetRange' => {}, 'OrgId' => {},
        'OrgAbuseHandle' => {}, 'OrgDNSHandle' => {}, 'OrgNOCHandle' => {},
        'OrgRoutingHandle' => {}, 'OrgTechHandle' => {}, 'RAbuseHandle' => {},
        'ReferralServer' => {}, 'ResourceLink' => {}, 'RNOCHandle' => {},
        'RTechHandle' => {});
    for (@lines) {
        if (m/\A\Q# ARIN WHOIS data and services\Q/) {
            last if $disclaimer;
            $disclaimer++;
            next;
        }
        next if m/\A#/;
        # Multiple start-end blocks are not handled properly.
        if (m/\A\z/) {
            next unless defined $section_name;
            if ($section_name eq 'NetRange') {
                ${$objects{$section_name}}{$section->{'NetRange'}} = $section;
            } elsif ($section_name eq 'OrgName') {
                ${$objects{'OrgId'}}{$section->{'OrgId'}} = $section;
            } elsif ($section_name eq 'CustName' or
                    $section_name eq 'OrgAbuseHandle' or
                    $section_name eq 'OrgDNSHandle' or
                    $section_name eq 'OrgNOCHandle' or
                    $section_name eq 'OrgRoutingHandle' or
                    $section_name eq 'OrgTechHandle' or
                    $section_name eq 'RAbuseHandle' or
                    $section_name eq 'ResourceLink' or
                    $section_name eq 'ReferralServer' or
                    $section_name eq 'RNOCHandle' or
                    $section_name eq 'RTechHandle') {
                ${$objects{$section_name}}{$section->{$section_name}} = $section;
            } else {
                die "Unknown ARIN object type $section_name.\n";
            }
            $section = undef;
            $section_name= undef;
            next;
        }
        if (m/\A([\S]+):\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if (!defined $section_name) {
                $section_name = $key;
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                $section->{$key} = $value;
            }
            if ($key eq 'CIDR' and $value =~ m{/(\d+)}) {
                $section->{'prefixlen'} = $1;
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (inetnum, redirect) of hash of objects by their
# identifiers.
sub parse_iana {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text);
    my $section;
    my %objects = ('inetnum' => {}, 'refer' => undef);
    # Multiple inetnums are not handled properly.
    for (@lines) {
        next if m/\A%/;
        if (m/\A([\S]+):\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                $section->{$key} = $value;
            }
            if ($key eq 'inetnum') {
                $section->{'prefixlen'} = netrange2prefixlen($value);
            }
        }
    }
    if (exists $section->{'refer'}) {
        my $refer = delete $section->{'refer'};
        $refer = shift @$refer if ref $refer ne '';
        $objects{'refer'} = $refer;
    }
    if (exists $section->{'inetnum'}) {
        $objects{'inetnum'}->{$section->{'inetnum'}} = $section;
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (Contact Information, Group Contact
# Information, Network Information) of hash of objects by their
# identifiers.
sub parse_jpnic {
    my ($self, $text) = @_;
    my @lines = (split(/\n/, $text), "");
    my $in_block;
    my $section_name;
    my $section;
    my %objects = ('Contact Information' => {},
        'Group Contact Information' => {}, 'Network Information' => {});
    for (@lines) {
        next if m/\A\[ [^]]+ ]\z/;
        if (m/\A([^]]+ Information):\s*\z/) {
            $in_block = 1;
            $section = undef;
            $section_name = $1;
            next;
        }
        # Multiple blocks are not handled properly.
        if ($in_block and m/\A\z/) {
            next unless defined $section_name;
            if ($section_name eq 'Contact Information') {
                ${$objects{$section_name}}{$section->{'JPNIC Handle'}} = $section;
            } elsif ($section_name eq 'Group Contact Information') {
                ${$objects{$section_name}}{$section->{'Group Handle'}} = $section;
            } elsif ($section_name eq 'Network Information') {
                ${$objects{$section_name}}{$section->{'Network Number'}} = $section;
            } else {
                die "Unknown JPNIC object type $section_name.\n";
            }
            $in_block = 0;
            $section = undef;
            $section_name= undef;
            next;
        }
        if ($in_block and m/\A(?:\w\. )?\[([^]]+)]\s+(\S.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if (!defined $section_name) {
                $section_name = $key;
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                $section->{$key} = $value;
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (IPv4 Address) of hash of objects by their
# identifiers.
sub parse_krnic {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text);
    my $in_block;
    my $section_name;
    my $section;
    my %objects = ('IPv4 Address' => {});
    for (@lines) {
        next if m/\A#/;
        if (m/\A\Q[ Network Information ]\E/) {
            $in_block = 1;
            next;
        }
        if ($in_block and m/\A-/) {
            next unless defined $section_name;
            if ($section_name eq 'IPv4 Address') {
                ${$objects{$section_name}}{$section->{$section_name}} = $section;
            } else {
                die "Unknown KRNIC object type $section_name.\n";
            }
            $in_block = 0;
            $section = undef;
            $section_name= undef;
            next;
        }
        if ($in_block and m/\A(\w[^:]+?)\s+:\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if (!defined $section_name) {
                $section_name = $key;
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                $section->{$key} = $value;
            }
            if ($key eq 'IPv4 Address' and $value =~ m{/(\d+)}) {
                $section->{'prefixlen'} = $1;
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (inetnum, nic-hdl) of hash of
# objects by their identifiers.
sub parse_lacnic {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text);
    my $section_name;
    my $section;
    my %objects = ('inetnum' => {}, 'nic-hdl' => {});
    for (@lines) {
        next if m/\A%/;
        if (m/\A\z/ or m/\A\QAnswer from RIR truncated\E\z/) {
            next unless defined $section_name;
            if ($section_name eq 'inetnum') {
                ${$objects{$section_name}}{$section->{'inetnum'}} = $section;
            } elsif ($section_name eq 'nic-hdl' or $section_name eq 'nic-hdl-br') {
                ${$objects{'nic-hdl'}}{$section->{$section_name}} = $section;
            } else {
                die "Unknown LACNIC object type $section_name.\n";
            }
            $section = undef;
            $section_name= undef;
            next;
        }
        if (m/\A([\S]+):\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if (!defined $section_name) {
                $section_name = $key;
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                # inetrev, nserver, nsstat, and nslastaa are not handled properly
                if ($key eq 'admin-c' or $key eq 'tech-c') {
                    $section->{$key} = [$value];
                } else { 
                    $section->{$key} = $value;
                }
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (inet6num, inetnum, organisation, role, person,
# route6, route) of hash of objects by their identifiers.
sub parse_ripe {
    my ($self, $text) = @_;
    my @lines = split(/\n/, $text);
    my $section_name;
    my $section;
    my %abuse_from_comments;
    my %objects = ('inet6num', => {}, 'inetnum' => {}, 'organisation' => {},
        'role' => {}, 'person' => {}, 'route6' => {}, 'route' => {});
    for (@lines) {
        if (m/\A\Q% Abuse contact for '\E([^-]+ - [^-]+)
                \Q' is '\E(\S+@\S+)\Q'\E/x) {
                $abuse_from_comments{$1} = $2;
            next;
        }
        next if m/\A%/;
        if (m/\A\z/) {
            next unless defined $section_name;
            if ($section_name eq 'inet6num' or $section_name eq 'inetnum') {
                if (exists $abuse_from_comments{$section->{$section_name}}) {
                    $section->{'abuse-from-comments'} =
                        $abuse_from_comments{$section->{$section_name}};
                }
                ${$objects{$section_name}}{$section->{$section_name}} = $section;
            } elsif ($section_name eq 'role' or $section_name eq 'person') {
                ${$objects{$section_name}}{$section->{'nic-hdl'}} = $section;
            } elsif ($section_name eq 'route6' or $section_name eq 'route' or
                    $section_name eq 'organisation') {
                ${$objects{$section_name}}{$section->{$section_name}} = $section;
            } else {
                die "Unknown RIPE object type $section_name.\n";
            }
            $section = undef;
            $section_name= undef;
            next;
        }
        if (m/\A([\S]+):\s+(.+)/) {
            my ($key, $value) = ($1, $2);
            #warn "$key=$value\n";
            if (!defined $section_name) {
                $section_name = $key;
            }
            if (exists $section->{$key}) {
                if (ref $section->{$key} eq '') {
                    $section->{$key} = [$section->{$key}, $value];
                } else {
                    push @{$section->{$key}}, $value;
                }
            } else {
                if ($key eq 'admin-c' or $key eq 'tech-c') {
                    $section->{$key} = [$value];
                } else { 
                    $section->{$key} = $value;
                }
            }
        }
    }
    $self->debug(\%objects);
    return \%objects;
}


# Return a reference to a hash (Netblock) of hash of objects by their
# identifiers.
sub parse_twnic {
    my ($self, $text) = @_;
    my @lines = (split(/\n/, $text), '');
    my $section = {};
    my $key;
    my %objects = ('Netblock' => {});
    # Multiple Netname blocks are not supported.
    for (@lines) {
        if (m/\A\z/) {
            $key = undef;
            next;
        }
        if (m/\A   (\w[^:]+):\s+(\w.*)/) {
            my ($key, $value) = ($1, $2);
            $section->{$key} = $value;

            if ($key eq 'Netblock' and $value =~ m{/(\d+)}) {
                $section->{'prefixlen'} = $1;
            }
        } elsif (m/\A   (\w[^:]+):\z/) {
            $key = $1;
        } elsif (defined $key and m/\A      (\w.*)\z/) {
            if (exists $section->{$key}) {
                push @{$section->{$key}}, $1;
            } else {
                $section->{$key} = [$1];
            }
        }
    }
    if (exists $section->{'Netblock'}) {
        ${$objects{'Netblock'}}{$section->{'Netblock'}} = $section;
    } else {
        die "TWNIC object is missing a Netblock record.\n";
    }
    $self->debug(\%objects);
    return \%objects;
}


# Sort objects by prefixlen in descending order.
sub sort_by_prefixlen {
    my $objects = shift;
    reverse sort({$a->{'prefixlen'} <=> $b->{'prefixlen'}} values %$objects);
}


# Search in Afrinic output
sub search_afrinic {
    my ($self, $output) = @_;

    my $objects = $self->parse_afrinic($output);
    if (exists $objects->{'redirect'}) {
        return [$objects->{'redirect'}];
    }
    my @handles;
    for my $inetnum (values %{$objects->{'inetnum'}}) {
        if (exists $inetnum->{'tech-c'}) {
            push @handles, @{$inetnum->{'tech-c'}};
        }
        if (exists $inetnum->{'admin-c'}) {
            push @handles, @{$inetnum->{'admin-c'}};
        }
    }
    @handles = List::MoreUtils::uniq(@handles);
    for my $handle (@handles) {
        if (exists $objects->{person}->{$handle}) {
            my $person = $objects->{person}->{$handle};
            if (exists $person->{'abuse-mailbox'}) {
                return $person->{'abuse-mailbox'};
            }
            if (exists $person->{'e-mail'}) {
                return first_item($person->{'e-mail'});
            }
            if (exists $person->{'source'} and
                    $person->{'source'} =~ m/Filtered/) {
                my $unfiltered_objects = $self->parse_afrinic(
                    $self->query('whois.afrinic.net', "-B $handle")
                );
                if (exists $unfiltered_objects->{'person'}->{$handle}->{'e-mail'}) {
                    return first_item($unfiltered_objects->{'person'}->{$handle}->{'e-mail'});
                }
            }
        }
        if (exists $objects->{role}->{$handle}) {
            my $role = $objects->{role}->{$handle};
            if (exists $role->{'abuse-mailbox'}) {
                return $role->{'abuse-mailbox'};
            }
            if (exists $role->{'tech-c'}) {
                warn "Looking up AfriNIC role contacts not yet implemented: $handle\n";
            }
            if (exists $role->{'e-mail'}) {
                return first_item($role->{'e-mail'});
            }
            if (exists $role->{'source'} and
                $role->{'source'} =~ m/Filtered/) {
                warn "Looking up unfiltered AfriNIC role objects not yet implemented: $handle\n";
            }
        }
    }
    return undef;
}


# Search in APNIC output.
sub search_apnic {
    my ($self, $output) = @_;

    my $objects = $self->parse_apnic($output);
    my @handles;
    for my $inetnum (sort_by_prefixlen($objects->{'inet6num'}),
            sort_by_prefixlen($objects->{'inetnum'})) {
        if (exists $inetnum->{'netname'}
                and $inetnum->{'netname'} eq 'ERX-NETBLOCK') {
            return ['whois.afrinic.net', 'whois.arin.net',
                    'whois.lacnic.net', 'whois.ripe.net'];
        }
        if (exists $inetnum->{'source'}) {
            if ($inetnum->{'source'} eq 'JPNIC') {
                return ['whois.nic.ad.jp'];
            }
            if ($inetnum->{'source'} eq 'KRNIC') {
                return ['whois.kisa.or.kr'];
            }
            if ($inetnum->{'source'} eq 'TWNIC') {
                return ['whois.twnic.net'];
            }
        }
        if (exists $inetnum->{'abuse-c'}) {
            push @handles, $inetnum->{'abuse-c'};
        }
        if (exists $inetnum->{'mnt-irt'}) {
            if ($inetnum->{'mnt-irt'} eq 'IRT-STUB-AP') {
                # Stub delegation to a foreign RIR
                if (exists $inetnum->{'descr'} and
                        $inetnum->{'descr'} =~ m/\ATransferred to the (RIPE) region on/) {
                    if ($1 eq 'RIPE') {
                        return ['whois.ripe.net'];
                    } else {
                        warn "Redirecting this stub delegation ($_) is not yet implemented\n";
                    }
                }
            } elsif ($inetnum->{'mnt-irt'} eq 'IRT-VNNIC-AP') {
                # VNNIC is a NIR, without a WHOIS server. Ignore it.
                if (exists $inetnum->{'remarks'}) {
                    for (@{$inetnum->{'remarks'}}) {
                        return $1 if m/\Asend spam and abuse report to (\S+@\S+)\z/;
                    }
                }
            } else {
                push @handles, $inetnum->{'mnt-irt'};
            }
        }
        if (exists $inetnum->{'tech-c'}) {
            push @handles, @{$inetnum->{'tech-c'}};
        }
        if (exists $inetnum->{'admin-c'}) {
            push @handles, @{$inetnum->{'admin-c'}};
        }
    }
    @handles = List::MoreUtils::uniq(@handles);
    for my $handle (@handles) {
        $self->debug("Searching APNIC $handle object.\n");
        if (exists $objects->{irt}->{$handle}) {
            my $irt = $objects->{irt}->{$handle};
            if (exists $irt->{'remarks'} and
                    grep(m/(?:\QPlease contact the tech-c or admin-c of the network\E|
                        \QPlease note that TWNIC is not an ISP and is not empowered\E)/x,
                        @{$irt->{'remarks'}})) {
                next;
            }
            if (exists $irt->{'abuse-mailbox'}) {
                return $irt->{'abuse-mailbox'};
            }
            if (exists $irt->{'e-mail'}) {
                return first_item($irt->{'e-mail'});
            }
        }
        for my $type ('role', 'person') {
            if (exists $objects->{$type}->{$handle}) {
                my $object = $objects->{$type}->{$handle};
                if (exists $object->{'abuse-mailbox'}) {
                    return first_item($object->{'abuse-mailbox'});
                }
                if (exists $object->{'e-mail'}) {
                    return first_item($object->{'e-mail'});
                }
            }
        }
    }
    return undef;
}


# Search in ARIN output.
sub search_arin {
    my ($self, $output) = @_;

    my $objects = $self->parse_arin($output);
    for my $referral (keys %{$objects->{'ReferralServer'}}) {
        if ($referral =~ m{\Awhois://([^/]+)}) {
            return [$1];
        }
    }
    my @handles;
    for my $handle_type (qw(OrgAbuse RAbuse OrgTech RTech RNOC OrgNOC)) {
        for my $handle (values %{$objects->{$handle_type . 'Handle'}}) {
            if (exists $handle->{$handle_type . 'Email'}) {
                return $handle->{$handle_type . 'Email'};
            }
        }
    }
    return undef;
}


# Search in IANA output.
sub search_iana {
    my ($self, $output) = @_;

    my $objects = $self->parse_iana($output);
    if (defined $objects->{'refer'}) {
        return [$objects->{'refer'}];
    }
    return undef;
}


# Search in JPNIC output.
sub search_jpnic {
    my ($self, $output) = @_;

    my $objects = $self->parse_jpnic($output);
    my @handles;
    for my $network (values %{$objects->{'Network Information'}}) {
        for my $item ('Technical Contact', 'Administrative Contact') {
            if (exists $network->{$item}) {
                if (ref $network->{$item} eq '') {
                    push @handles, $network->{$item};
                } else {
                    push @handles, @{$network->{$item}};
                }
            }
        }
    }
    @handles = List::MoreUtils::uniq(@handles);
    for my $handle (@handles) {
        my @types = ('Contact Information', 'Group Contact Information');
        my $object;
        for my $type (@types) {
            if (exists $objects->{$type}->{$handle}) {
                $object = $objects->{$type}->{$handle};
                last;
            }
        }
        if (!defined $object) {
            my $new_objects = $self->parse_jpnic($self->query(
                    'whois.nic.ad.jp', $handle));
            for my $type (@types) {
                if (exists $new_objects->{$type}->{$handle}) {
                    $object = $new_objects->{$type}->{$handle};
                    last;
                }
            }
        }
        if (defined $object and exists $object->{'E-Mail'}) {
            return $object->{'E-Mail'};
        }
    }
    return undef;
}


# Search in KRNIC output.
sub search_krnic {
    my ($self, $output) = @_;

    my $objects = $self->parse_krnic($output);
    my @handles;
    for my $handle (sort_by_prefixlen($objects->{'IPv4 Address'})) {
        if (exists $handle->{'E-Mail'}) {
            return $handle->{'E-Mail'};
        }
    }
    return undef;
}


# Search in LACNIC output.
sub search_lacnic {
    my ($self, $output) = @_;

    my $objects = $self->parse_lacnic($output);
    my @handles;
    for my $inetnum (values %{$objects->{'inetnum'}}) {
        if (exists $inetnum->{'abuse-c'}) {
            push @handles, $inetnum->{'abuse-c'};
        }
        if (exists $inetnum->{'tech-c'}) {
            push @handles, @{$inetnum->{'tech-c'}};
        }
        if (exists $inetnum->{'owner-c'}) {
            push @handles, $inetnum->{'owner-c'};
        }
    }
    @handles = List::MoreUtils::uniq(@handles);
    for my $handle (@handles) {
        $self->debug("Searching LACNIC $handle object.\n");
        my $person;
        if (exists $objects->{'nic-hdl'}->{$handle}) {
            $person = $objects->{'nic-hdl'}->{$handle};
        } else {
            my $objects = $self->parse_lacnic($self->query(
                    'whois.lacnic.net', $handle));
            if (exists $objects->{'nic-hdl'}->{$handle}) {
                $person = $objects->{'nic-hdl'}->{$handle};
            }
        }
        if (defined $person and exists $person->{'e-mail'}) {
            return first_item($person->{'e-mail'});
        }
    }
    return undef;
}


# Search in RIPE output
sub search_ripe {
    my ($self, $output) = @_;

    my $objects = $self->parse_ripe($output);
    my @handles;
    for my $inetnum (values %{$objects->{'inet6num'}}, values %{$objects->{'inetnum'}}) {
        if (exists $inetnum->{'netname'} and
                $inetnum->{'netname'} eq 'NON-RIPE-NCC-MANAGED-ADDRESS-BLOCK') {
            return ['whois.arin.net', 'whois.apnic.net', 'whois.afrinic.net', 'whois.lacnic.net'];
        }
        for my $type (qw(abuse-c org tech-c admin-c)) {
            if (exists $inetnum->{$type}) {
                if (ref $inetnum->{$type} eq '') {
                    push @handles, $inetnum->{$type};
                } else {
                    push @handles, @{$inetnum->{$type}};
                }
            }
        }
    }
    @handles = List::MoreUtils::uniq(@handles);
    for my $handle (@handles) {
        $self->debug("Searching RIPE $handle object.\n");
        if (exists $objects->{organisation}->{$handle}) {
            my $organisation = $objects->{organisation}->{$handle};
            if (exists $organisation->{'abuse-mailbox'}) {
                return $organisation->{'abuse-mailbox'};
            }
            if (exists $organisation->{'abuse-c'}) {
                my $abuse_c = $organisation->{'abuse-c'};
                my $role;
                if (exists $objects->{'role'}->{$abuse_c}) {
                    $role = $objects->{'role'}->{$abuse_c};
                } else {
                    my $new_objects = $self->parse_ripe($self->query(
                            'whois.ripe.net', $abuse_c));
                    if (exists $new_objects->{'role'}->{$abuse_c}) {
                        $role = $new_objects->{'role'}->{$abuse_c};
                    }
                }
                if (defined $role and exists $role->{'abuse-mailbox'}) {
                    return $role->{'abuse-mailbox'};
                }
            }
            if (exists $organisation->{'tech-c'}) {
                warn "Looking up RIPE organisation contacts not yet implemented: $handle\n";
            }
            if (exists $organisation->{'e-mail'}) {
                return first_item($organisation->{'e-mail'});
            }
            if (exists $organisation->{'source'} and
                $organisation->{'source'} =~ m/Filtered/) {
                warn "Looking up unfiltered RIPE organisation objects not yet implemented: $handle\n";
            }
        } elsif (exists $objects->{person}->{$handle}) {
            my $person = $objects->{person}->{$handle};
            if (exists $person->{'abuse-mailbox'}) {
                return $person->{'abuse-mailbox'};
            }
            if (exists $person->{'e-mail'}) {
                return first_item($person->{'e-mail'});
            }
            if (exists $person->{'source'} and
                    $person->{'source'} =~ m/Filtered/) {
                my $unfiltered_objects = $self->parse_ripe($self->query(
                        'whois.ripe.net', "-B $handle"));
                if (exists $unfiltered_objects->{'person'}->{$handle}->{'e-mail'}) {
                    return first_item($unfiltered_objects->{'person'}->{$handle}->{'e-mail'});
                }
            }
        } elsif (exists $objects->{role}->{$handle}) {
            my $role = $objects->{role}->{$handle};
            if (exists $role->{'abuse-mailbox'}) {
                return $role->{'abuse-mailbox'};
            }
            if (exists $role->{'tech-c'}) {
                warn "Looking up RIPE role contacts not yet implemented: $handle\n";
            }
            if (exists $role->{'e-mail'}) {
                return first_item($role->{'e-mail'});
            }
            if (exists $role->{'source'} and
                $role->{'source'} =~ m/Filtered/) {
                my $unfiltered_objects = $self->parse_ripe($self->query(
                        'whois.ripe.net', "-B $handle"));
                if (exists $unfiltered_objects->{'role'}->{$handle}->{'e-mail'}) {
                    return first_item($unfiltered_objects->{'role'}->{$handle}->{'e-mail'});
                }
            }
        } else {
            # The contact was referred, but the object was missing from the
            # reply. Query for referred object explicitly.
            my $objects = $self->parse_ripe($self->query(
                    'whois.ripe.net', $handle));
            for my $type ('role', 'person') {
                my $object = $objects->{$type}->{$handle};
                if (exists $object->{'abuse-mailbox'}) {
                    return $object->{'abuse-mailbox'};
                }
                if (exists $object->{'e-mail'}) {
                    return first_item($object->{'e-mail'});
                }
                if (exists $object->{'source'} and
                        $object->{'source'} =~ m/Filtered/) {
                    my $unfiltered_objects = $self->parse_ripe($self->query(
                            'whois.ripe.net', "-B $handle"));
                    if (exists $unfiltered_objects->{$type}->{$handle}->{'e-mail'}) {
                        return first_item($unfiltered_objects->{$type}->{$handle}->{'e-mail'});
                    }
                }
            }
        }
    }
    for my $inetnum (values %{$objects->{'inet6num'}}, values %{$objects->{'inetnum'}}) {
        if (exists $inetnum->{'abuse-from-comments'}) {
            return $inetnum->{'abuse-from-comments'};
        }
    }
    return undef;
}


# Search in TWNIC output.
sub search_twnic {
    my ($self, $output) = @_;

    my $objects = $self->parse_twnic($output);
    my @handles;
    for my $handle (sort_by_prefixlen($objects->{'Netblock'})) {
        for my $type ('Technical contact', 'Administrator contact') {
            if (exists $handle->{$type} and defined $handle->{$type}->[0]) {
                return $handle->{$type}->[0];
            }
        }
    }
    return undef;
}


=head2 search(I<IP_ADDRESS>)

Search an abuse address for an IP adress. Return exactly one e-mail address,
or undef if no address was found. It can die if an error is encountered.

=cut

sub search {
    my ($self, $ip) = @_;
    my @servers = (address2rir($ip) // 'whois.iana.org');
    my %visited;

    while (@servers) {
        my $server = shift @servers;
        my $output = $self->query($server, $ip);
        $visited{$server} = undef;
        my $answer;

        if ($output =~ m/\A\Q% This is the AfriNIC Whois server.\E/) {
            $answer = $self->search_afrinic($output);
        } elsif ($output =~ m/\A\Q% [whois.apnic.net]\E/) {
            $answer = $self->search_apnic($output);
        } elsif ($output =~ m/\A\n#\n\Q# ARIN WHOIS data and services\E/) {
            $answer = $self->search_arin($output);
        } elsif ($output =~ m/\A\Q% IANA WHOIS server\E/) {
            $answer = $self->search_iana($output);
        } elsif ($output =~ m/\A\Q[ JPNIC database provides information\E/) {
            $answer = $self->search_jpnic($output);
        } elsif ($output =~ m/\QKRNIC is not an ISP but a National Internet Registry\E/) {
            $answer = $self->search_krnic($output);
        } elsif ($output =~ m/(?:\QLACNIC resource\E|\QJoint Whois - whois.lacnic.net\E|\QCopyright (c) Nic.br\E)/) {
            $answer = $self->search_lacnic($output);
        } elsif ($output =~ m/\QThis is the RIPE Database query service\E/) {
            $answer = $self->search_ripe($output);
        } elsif ($output =~ m/\A\n\Q   Netname: \E/ or
                (defined $server and $server eq 'whois.twnic.net')) {
            $answer = $self->search_twnic($output);
        } else {
            die "This RIR is not supported:\n$output";
        }

        if (defined $answer and ref $answer eq '') {
            return $answer;
        }
        @servers = List::MoreUtils::uniq(grep(!exists $visited{$_}, @$answer), @servers);
    }
    return undef;
}

1;

=head1 SEE ALSO

RFC 3912, whois(1)

=head1 AUTHOR

Petr Písař L<mailto:petr.pisar@atlas.cz>

=head1 LICENSE

Copyright © 2020, 2021  Petr Písař <petr.pisar@atlas.cz>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

=cut

