use Test::More tests => 4;
use App::Abuser ();
require './t/Stub.pm';
$Stub::server = 'whois.ripe.net';

is(App::Abuser->new->search('192.0.2.1'), 'good@example.com', 'inetnum with org with abuse-c with abuse-mailbox');
is(App::Abuser->new->search('192.0.2.2'), 'good@example.com', 'inetnum with abuse comment only');
is(App::Abuser->new->search('192.0.2.5'), 'good@example.com', 'inetnum with abuse-c which needs a new query');
is(App::Abuser->new->search('2001:db8::1'), 'good@example.com', 'inet6num with org with a separate abuse-c with a abuse-mailbox');
