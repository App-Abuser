use Test::More;
use App::Abuser ();
use strict;
use warnings;

# Parse IANA allocation registry
my @allocations;
{
    open(my $fh, '<', 't/ipv4-address-space.txt') or
        die "Could not open 't/ipv4-address-space.txt': $!";
    while (<$fh>) {
        if (m{\A   (\d\d\d)/8  .*\d\d\d\d-\d\d {1,2}(\w\S+|  )}) {
            my ($address, $whois) = ($1, $2);
            $address = sprintf("%d.0.0.0", $address);
            $whois = undef if $whois eq '  ';
            push @allocations, [$address, $whois];
        }
    }
    close($fh)
        or die "Could not read 't/ipv4-address-space.txt'";

    open($fh, '<', 't/ipv6-unicast-address-assignments.txt') or
        die "Could not open 't/ipv6-unicast-address-assignments.txt': $!";
    while (<$fh>) {
        if (m{\A   ([0-9a-f:]+)/\d+ .*\d\d\d\d-\d\d(?:-\d\d) (\w\S+|  )}) {
            my ($address, $whois) = ($1, $2);
            $whois = undef if $whois eq '  ';
            push @allocations, [$address, $whois];
        }
    }
    close($fh)
        or die "Could not read 't/ipv6-unicast-address-assignments.txt'";
}

plan tests => 2 * 4 + @allocations;

# Check for few positive and negative examples with various prefix length to
# verify the algorithm.
is(App::Abuser::address2rir('0.0.0.0'), undef, '0.0.0.0 is reserved');
is(App::Abuser::address2rir('0.0.0.1'), undef, '0.0.0.1 is reserved');
is(App::Abuser::address2rir('1.0.0.0'), 'whois.apnic.net',
    '1.0.0.0 allocated to APNIC');
is(App::Abuser::address2rir('1.0.0.1'), 'whois.apnic.net',
    '1.0.0.1 allocated to APNIC');
is(App::Abuser::address2rir('2001::0'), 'whois.iana.org',
    '2001::0 allocated to IANA');
is(App::Abuser::address2rir('2001::1'), 'whois.iana.org',
    '2001::1 allocated to IANA');
is(App::Abuser::address2rir('5f00::0'), undef, '5f00::0 is reserved');
is(App::Abuser::address2rir('5f00::1'), undef, '5f00::1 is reserved');

# Check for all IANA data
for my $allocation (@allocations) {
    is(App::Abuser::address2rir($allocation->[0]), $allocation->[1],
        "$allocation->[0] matches IANA registry");
}
