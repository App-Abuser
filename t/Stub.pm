package Stub;
use strict;
use warnings;
use utf8;

my $examples = {
    'whois.afrinic.net' => {
        '192.0.2.3' => <<'OUTPUT',
% This is the AfriNIC Whois server.
% The AFRINIC whois database is subject to  the following terms of Use. See https://afrinic.net/whois/terms

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
descr:          X
admin-c:        ROLE-2
tech-c:         ROLE-2
remarks:        Email address for spam or abuse complaints: bad@example.com
country:        X
mnt-by:         X
mnt-lower:      X
mnt-irt:        IRT-2
status:         X
last-modified:  X
source:         APNIC

irt:            IRT-2
address:        X
address:        X
e-mail:         bad@example.com
abuse-mailbox:  bad@example.com
admin-c:        ROLE-2
tech-c:         ROLE-2
auth:           # Filtered
remarks:        X
mnt-by:         X
last-modified:  X
source:         APNIC

role:           X
address:       	X
address:        X
address:        X
country:        X
phone:          X
fax-no:         X
e-mail:         bad@example.com
admin-c:        X
tech-c:         X
nic-hdl:        ROLE-2
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '192.0.2.0 - 192.0.2.255'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
remarks:        Email address for spam or abuse complaints : bad@example.com
country:        X
admin-c:        ROLE-3
tech-c:         RILE-3
remarks:        This information has been partially mirrored by APNIC from
remarks:        JPNIC. To obtain more specific information, please use the
remarks:        JPNIC WHOIS Gateway at
remarks:        http://www.nic.ad.jp/en/db/whois/en-gateway.html or
remarks:        whois.nic.ad.jp for WHOIS client. (The WHOIS client
remarks:        defaults to Japanese output, use the /e switch for English
remarks:        output)
last-modified:  X
source:         JPNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-JP3)





OUTPUT
    },
    'whois.apnic.net' => {
        '192.0.2.0' => <<'OUTPUT',
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
descr:          X
country:        X
org:            ORG-1
admin-c:        ROLE-1
tech-c:         ROLE-1
abuse-c:        ROLE-2
status:         ASSIGNED PORTABLE
mnt-by:         X
mnt-routes:     X
mnt-irt:        IRT-1
last-modified:  1970-01-01T00:00:00Z
source:         APNIC

irt:            IRT-1
address:        X
address:        X
address:        X
e-mail:         bad@example.com
abuse-mailbox:  bad@example.com
admin-c:        ROLE-1
tech-c:         ROLE-1
auth:           # Filtered
remarks:        X
mnt-by:         X
last-modified:  1970-01-01T00:00:00Z
source:         APNIC

organisation:   ORG-1
org-name:       X
country:        X
address:        X
e-mail:         bad@example.com
mnt-ref:        X
mnt-by:         X
last-modified:  1970-01-01T00:00:00Z
source:         APNIC

role:           X
address:        X
address:        X
address:        X
country:        X
phone:          X
e-mail:         bad@example.com
admin-c:        ROLE-1
tech-c:         ROLE-1
nic-hdl:        ROLE-2
remarks:        X
abuse-mailbox:  good@example.com
mnt-by:         X
last-modified:  1970-01-01T00:00:00Z
source:         APNIC

role:           X
address:        X
address:        X
address:        X
country:        X
phone:          X
fax-no:         X
e-mail:         bad@example.com
nic-hdl:        ROLE-1
tech-c:         X
admin-c:        X
mnt-by:         X
last-modified:  1970-01-01T00:00:00Z
source:         APNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-UK4)


OUTPUT
        '192.0.2.3' => <<'OUTPUT',
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
descr:          X
admin-c:        ROLE-2
tech-c:         ROLE-2
remarks:        Email address for spam or abuse complaints: bad@example.com
country:        X
mnt-by:         X
mnt-lower:      X
mnt-irt:        IRT-2
status:         X
last-modified:  X
source:         APNIC

irt:            IRT-2
address:        X
address:        X
e-mail:         bad@example.com
abuse-mailbox:  bad@example.com
admin-c:        ROLE-2
tech-c:         ROLE-2
auth:           # Filtered
remarks:        X
mnt-by:         X
last-modified:  X
source:         APNIC

role:           X
address:       	X
address:        X
address:        X
country:        X
phone:          X
fax-no:         X
e-mail:         bad@example.com
admin-c:        X
tech-c:         X
nic-hdl:        ROLE-2
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '192.0.2.0 - 192.0.2.255'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
remarks:        Email address for spam or abuse complaints : bad@example.com
country:        X
admin-c:        ROLE-3
tech-c:         ROLE-3
remarks:        This information has been partially mirrored by APNIC from
remarks:        JPNIC. To obtain more specific information, please use the
remarks:        JPNIC WHOIS Gateway at
remarks:        http://www.nic.ad.jp/en/db/whois/en-gateway.html or
remarks:        whois.nic.ad.jp for WHOIS client. (The WHOIS client
remarks:        defaults to Japanese output, use the /e switch for English
remarks:        output)
last-modified:  X
source:         JPNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-JP3)


OUTPUT
        '192.0.2.4' => <<'OUTPUT',
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
country:        X
tech-c:         person-2
admin-c:        person-3
status:         X
mnt-by:         X
last-modified:  X
source:         APNIC

person:         X
nic-hdl:        PERSON-3
e-mail:         bad@example.com
address:        X
address:        X
phone:          X
fax-no:         X
country:        X
mnt-by:         X
last-modified:  X
source:         APNIC

person:         X
nic-hdl:        PERSON-2
e-mail:         good@example.com
address:        X
address:        X
phone:          X
fax-no:         X
country:        X
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '192.0.2.0/24AS0002'

route:          192.0.2.0/24
descr:          X
origin:         AS0002
mnt-by:         X
last-modified:  X
source:         APNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-JP3)


OUTPUT
        '192.0.2.7' => <<'OUTPUT',
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
descr:          X
country:        X
admin-c:        ROLE-5
tech-c:         ROLE-5
status:         X
remarks:        X
mnt-by:         X
mnt-irt:        IRT-JPNIC-JP
mnt-lower:      X
last-modified:  X
source:         APNIC

irt:            IRT-JPNIC-JP
address:        X
address:        X
e-mail:         bad@example.com
abuse-mailbox:  bad@example.com
admin-c:        ROLE-5
tech-c:         ROLE-5
auth:           X
remarks:        X
mnt-by:         X
last-modified:  X
source:         APNIC

role:           X
address:        X
address:        X
address:        X
country:        X
phone:          X
fax-no:         X
e-mail:         bad@example.com
admin-c:        X
tech-c:         X
nic-hdl:        ROLE-5
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '192.0.2.0 - 192.0.2.255'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
country:        X
admin-c:        ROLE-6
tech-c:         ROLE-6
remarks:        This information has been partially mirrored by APNIC from
remarks:        JPNIC. To obtain more specific information, please use the
remarks:        JPNIC WHOIS Gateway at
remarks:        http://www.nic.ad.jp/en/db/whois/en-gateway.html or
remarks:        whois.nic.ad.jp for WHOIS client. (The WHOIS client
remarks:        defaults to Japanese output, use the /e switch for English
remarks:        output)
last-modified:  X
source:         JPNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-UK3)


OUTPUT
        '192.0.2.8' => <<'OUTPUT',
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.127'

% Abuse contact for '192.0.2.0 - 192.0.2.127' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.127
netname:        X
descr:          Transferred to the RIPE region on 1970-01-01T00:00:00Z.
country:        ZZ
admin-c:        STUB-AP
tech-c:         STUB-AP
status:         X
mnt-by:         X
mnt-irt:        IRT-STUB-AP
last-modified:  X
source:         APNIC

irt:            IRT-STUB-AP
address:        X
e-mail:         bad@example.com
abuse-mailbox:  bad@example.com
admin-c:        STUB-AP
tech-c:         STUB-AP
remarks:        X
remarks:        X
remarks:        X
remarks:        X
mnt-by:         X
auth:           # Filtered
last-modified:  X
source:         APNIC

person:         X
address:        X
country:        X
phone:          X
e-mail:         bad@example.com
nic-hdl:        STUB-AP
remarks:        X
mnt-by:         X
last-modified:  X
source:         APNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-UK3)


OUTPUT
        '192.0.2.9' => <<'OUTPUT',
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contacts for '192.0.2.0 - 192.0.2.255' are 'bad@example.com', 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
country:        ZZ
admin-c:        PERSON-6
tech-c:         PERSON-6
abuse-c:        ROLE-10
status:         X
mnt-by:         X
mnt-irt:        IRT-3
last-modified:  X
source:         APNIC

irt:            IRT-3
address:        X
address:        X
address:        X
address:        X
e-mail:         bad@example.com
e-mail:         bad@example.com
abuse-mailbox:  bad@example.com
abuse-mailbox:  bad@example.com
admin-c:        PERSON-6
tech-c:         PERSON-6
auth:           # Filtered
remarks:        X
remarks:        X
mnt-by:         X
last-modified:  X
source:         APNIC

role:           X
address:        X
address:        X
address:        X
address:        X
country:        ZZ
phone:          X
e-mail:         bad@example.com
e-mail:         bad@example.com
admin-c:        PERSON-6
tech-c:         PERSON-6
nic-hdl:        ROLE-10
remarks:        X
abuse-mailbox:  good@example.com
abuse-mailbox:  stillgood@example.com
mnt-by:         X
last-modified:  X
source:         APNIC

person:         X
address:        X
address:        X
country:        ZZ
phone:          X
e-mail:         bad@example.com
e-mail:         bad@example.com
nic-hdl:        PERSON-6
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '192.0.2.0/24AS0003'

route:          192.0.2.0/24
origin:         AS0003
descr:          X
                X
                X
                X
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '192.0.2.0/24AS0004'

route:          192.0.2.0/24
origin:         AS0004
descr:          X
                X
                X
                X
mnt-by:         X
last-modified:  X
source:         APNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-UK4)


OUTPUT
        '2001:db8::2' => <<'OUTPUT'
% [whois.apnic.net]
% Whois data copyright terms    http://www.apnic.net/db/dbcopyright.html

% Information related to '2001:db8::/32'

% No abuse contact registered for 2001:db8::/32

inet6num:       2001:db8::/32
netname:        X
descr:          X
country:        X
admin-c:        ROLE-8
tech-c:         ROLE-8
mnt-by:         X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
status:         X
last-modified:  X
source:         APNIC

role:           X
address:        X
address:        X
address:        X
country:        X
phone:          X
fax-no:         X
e-mail:         bad@example.com
admin-c:        X
tech-c:         X
nic-hdl:        ROLE-8
mnt-by:         X
last-modified:  X
source:         APNIC

% Information related to '2001:0db8::/32'

inet6num:       2001:0db8::/32
netname:        X
descr:          X
remarks:        X
country:        X
admin-c:        ROLE-9
tech-c:         ROLE-6
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
last-modified:  X
source:         JPNIC

% This query was served by the APNIC Whois Service version 1.88.15-SNAPSHOT (WHOIS-UK4)


OUTPUT
    },
    'whois.arin.net' => {
    },
    'whois.iana.org' => {
        '0.0.0.0' => <<'OUTPUT',
% IANA WHOIS server
% for more information on IANA, visit http://www.iana.org
% This query returned 1 object

inetnum:      0.0.0.0 - 0.255.255.255
organisation: IANA - Local Identification
status:       RESERVED

remarks:      0.0.0.0/8 reserved for self-identification [RFC1122],
remarks:      section 3.2.1.3. Reserved by protocol. For authoritative
remarks:      registration, seeiana-ipv4-special-registry.

changed:      1981-09
source:       IANA

OUTPUT
    },
    'whois.kisa.or.kr' => {
        '192.0.2.3' => <<'OUTPUT',
query : 192.0.2.3


# KOREAN(UTF8)

조회하신 IPv4주소는 한국인터넷진흥원으로부터 아래의 관리대행자에게 할당되었으며, 할당 정보는 다음과 같습니다.

[ 네트워크 할당 정보 ]
IPv4주소           : 192.0.2.0 - 192.0.2.255 (/24)
기관명             : X
서비스명           : X
주소               : X
우편번호           : X
할당일자           : X

이름               : X
전화번호           : X
전자우편           : bad@example.com

조회하신 IPv4주소는 위의 관리대행자로부터 아래의 사용자에게 할당되었으며, 할당 정보는 다음과 같습니다.
--------------------------------------------------------------------------------


[ 네트워크 할당 정보 ]
IPv4주소           : 192.0.2.0 - 192.0.2.127 (/25)
기관명             : X
네트워크 구분      : X
주소               : X
우편번호           : X
할당내역 등록일    : X

이름               : X
전화번호           : X
전자우편           : good@example.com


# ENGLISH

KRNIC is not an ISP but a National Internet Registry similar to APNIC.

[ Network Information ]
IPv4 Address       : 192.0.2.0 - 192.0.2.255 (/24)
Organization Name  : X
Service Name       : X
Address            : X
Zip Code           : X
Registration Date  : X

Name               : X
Phone              : X
E-Mail             : bad@example.com

--------------------------------------------------------------------------------

More specific assignment information is as follows.

[ Network Information ]
IPv4 Address       : 192.0.2.0 - 192.0.2.127 (/25)
Organization Name  : X
Network Type       : X
Address            : X
Zip Code           : X
Registration Date  : X

Name               : X
Phone              : X
E-Mail             : good@example.com


- KISA/KRNIC WHOIS Service -

OUTPUT
    },
    'whois.lacnic.net' => {
        '192.0.2.6' => <<'OUTPUT',

% Copyright (c) Nic.br
%  The use of the data below is only permitted as described in
%  full by the terms of use at https://registro.br/termo/en.html ,
%  being prohibited its distribution, commercialization or
%  reproduction, in particular, to use it for advertising or
%  any similar purpose.
%  1970-01-01T00:00:00+00:00 - IP: 2001:db8::

inetnum:     192.0.2.0/24
aut-num:     X
abuse-c:     CONTACT-1
owner:       X
ownerid:     X
responsible: X
country:     X
owner-c:     CONTACT-1
tech-c:      CONTACT-1
inetrev:     X
nserver:     X
nsstat:      X
nslastaa:    X
nserver:     X
nsstat:      X
nslastaa:    X
created:     X
changed:     X

nic-hdl-br:  CONTACT-1
person:      X
e-mail:      good@example.com
country:     X
created:     X
changed:     X

% Security and mail abuse issues should also be addressed to
% cert.br, http://www.cert.br/ , respectivelly to bad@example.com
% and bad@example.com
%
% whois.registro.br accepts only direct match queries. Types
% of queries are: domain (.br), registrant (tax ID), ticket,
% provider, CIDR block, IP and ASN.
OUTPUT
    },
    'whois.nic.ad.jp' => {
        '192.0.2.3' => <<'OUTPUT',
[ JPNIC database provides information regarding IP address and ASN. Its use   ]
[ is restricted to network administration purposes. For further information,  ]
[ use 'whois -h whois.nic.ad.jp help'. To only display English output,        ]
[ add '/e' at the end of command, e.g. 'whois -h whois.nic.ad.jp xxx/e'.      ]

Network Information:            
a. [Network Number]             192.0.2.0/25
b. [Network Name]               X
g. [Organization]               X
m. [Administrative Contact]     ROLE-3
n. [Technical Contact]          ROLE-3
p. [Nameserver]                 X
p. [Nameserver]                 X
[Assigned Date]                 X
[Return Date]                   
[Last Update]                   X
                                
Less Specific Info.
----------
X
                     [Allocation]                                192.0.2.0/24

More Specific Info.
----------
No match!!
OUTPUT
        'ROLE-3' => <<'OUTPUT',
[ JPNIC database provides information regarding IP address and ASN. Its use   ]
[ is restricted to network administration purposes. For further information,  ]
[ use 'whois -h whois.nic.ad.jp help'. To only display English output,        ]
[ add '/e' at the end of command, e.g. 'whois -h whois.nic.ad.jp xxx/e'.      ]

Group Contact Information:
[Group Handle]                  ROLE-3
[Group Name]                    X
[E-Mail]                        good@example.com
[Organization]                  X
[Division]                      
[TEL]                           X
[FAX]                           
[Last Update]                   X
                                bad@example.com
OUTPUT
        '192.0.2.7' => <<'OUTPUT',
[ JPNIC database provides information regarding IP address and ASN. Its use   ]
[ is restricted to network administration purposes. For further information,  ]
[ use 'whois -h whois.nic.ad.jp help'. To only display English output,        ]
[ add '/e' at the end of command, e.g. 'whois -h whois.nic.ad.jp xxx/e'.      ]

Network Information:            
a. [Network Number]             192.0.2.0/25
b. [Network Name]               X
g. [Organization]               X
m. [Administrative Contact]     ROLE-6
n. [Technical Contact]          ROLE-6
p. [Nameserver]                 X
[Assigned Date]                 X
[Return Date]                   
[Last Update]                   X
                                
Less Specific Info.
----------
X
                     [Allocation]                                192.0.2.0/24

More Specific Info.
----------
No match!!
OUTPUT
        'ROLE-6' => <<'OUTPUT',
[ JPNIC database provides information regarding IP address and ASN. Its use   ]
[ is restricted to network administration purposes. For further information,  ]
[ use 'whois -h whois.nic.ad.jp help'. To only display English output,        ]
[ add '/e' at the end of command, e.g. 'whois -h whois.nic.ad.jp xxx/e'.      ]

Contact Information: 
a. [JPNIC Handle]               ROLE-6
c. [Last, First]                X
d. [E-Mail]                     good@example.com
g. [Organization]               X
l. [Division]                   X
n. [Title]                      X
o. [TEL]                        X
p. [FAX]                        X
y. [Reply Mail]                 
[Last Update]                   X
                                bad@example.com
OUTPUT
        '2001:db8::2' => <<'OUTPUT',
[ JPNIC database provides information regarding IP address and ASN. Its use   ]
[ is restricted to network administration purposes. For further information,  ]
[ use 'whois -h whois.nic.ad.jp help'. To only display English output,        ]
[ add '/e' at the end of command, e.g. 'whois -h whois.nic.ad.jp xxx/e'.      ]

Network Information:            
[Network Number]                2001:0db8::/32
[Network Name]                  X
[Organization]                  X
[Administrative Contact]        ROLE-9
[Technical Contact]             ROLE-6
[Technical Contact]             X
[Technical Contact]             X
[Technical Contact]             X
[Technical Contact]             X
[Technical Contact]             X
[Nameserver]                    X
[Nameserver]                    X
[Assigned Date]                 X
[Return Date]                   
[Last Update]                   X
                                
Less Specific Info.
----------
Japan Network Information Center
X [Allocation]                              2001:0db8::/32

More Specific Info.
----------
No match!!
OUTPUT
    },
    'whois.ripe.net' => {
        '192.0.2.1' => <<'OUTPUT',
% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
descr:          X
country:        X
org:            ORG-2
admin-c:        PERSON-1
tech-c:         ROLE-1
status:         X
mnt-by:         X
remarks:        X
created:        X
last-modified:  X
source:         RIPE

organisation:   ORG-2
org-name:       X
org-type:       X
address:        X
address:        X
address:        X
address:        X
phone:          X
phone:          X
admin-c:        X
admin-c:        X
tech-c:         ROLE-1
abuse-c:        ROLE-1
mnt-ref:        X
mnt-ref:        X
mnt-by:         X
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

role:           X
address:        X
address:        X
address:        X
address:        X
address:        X
phone:          X
phone:          X
abuse-mailbox:  good@example.com
org:            ORG-2
admin-c:        X
tech-c:         X
tech-c:         X
nic-hdl:        ROLE-1
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

person:         X
address:        X
address:        X
address:        X
address:        X
address:        X
phone:          X
nic-hdl:        PERSON-1
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

% Information related to '192.0.2.0/24AS0001'

route:          192.0.2.0/24
descr:          X
origin:         AS0001
mnt-by:         X
remarks:        X
created:        X
last-modified:  X
source:         RIPE

% This query was served by the RIPE Database Query Service version 1.98 (WAGYU)


OUTPUT
        '192.0.2.2' => <<'OUTPUT',
% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'good@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
netname:        X
descr:          X
country:        X
admin-c:        PERSON-1
tech-c:         PERSON-2
status:         X
mnt-by:         X
mnt-domains:    X
created:        X
last-modified:  X
source:         RIPE

person:         X
address:        X
phone:          X
nic-hdl:        PERSON-1
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE

person:         X
address:        X
phone:          X
nic-hdl:        PERSON-2
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE

% Information related to '192.0.2.0/24AS00001'

route:          192.0.2.0/24
descr:          X
origin:         AS00001
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE

% This query was served by the RIPE Database Query Service version 1.98 (ANGUS)


OUTPUT
        '192.0.2.5' => <<'OUTPUT',
% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to '192.0.2.0 - 192.0.2.255'

% Abuse contact for '192.0.2.0 - 192.0.2.255' is 'bad@example.com'

inetnum:        192.0.2.0 - 192.0.2.255
abuse-c:        ROLE-4
netname:        X
country:        X
admin-c:        PERSON-4
tech-c:         PERSON-4
status:         X
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE

person:         X
address:        X
address:        X
address:        X
phone:          X
nic-hdl:        PERSON-4
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered
org:            X

% This query was served by the RIPE Database Query Service version 1.98 (BLAARKOP)


OUTPUT
        '192.0.2.8' => <<'OUTPUT',
% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to '192.0.2.8 - 192.0.2.127'

% Abuse contact for '192.0.2.8 - 192.0.2.127' is 'bad@example.com'

inetnum:        192.0.2.8 - 192.0.2.127
netname:        X
country:        X
org:            ORG-3
admin-c:        ROLE-5
tech-c:         ROLE-5
status:         X
mnt-by:         X
mnt-by:         X
mnt-lower:      X
mnt-domains:    X
mnt-routes:     X
created:        X
last-modified:  X
source:         RIPE # Filtered

organisation:   ORG-3
org-name:       X
org-type:       X
address:        X
address:        X
address:        X
address:        X
phone:          X
fax-no:         X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
abuse-c:        ROLE-5
mnt-ref:        X
mnt-ref:        X
mnt-by:         X
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

role:           X
address:        X
address:        X
address:        X
address:        X
phone:          X
fax-no:         X
abuse-mailbox:  good@example.com
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:        X
remarks:
remarks:        X
remarks:        X
remarks:        X
remarks:        X
org:            ORG-3
admin-c:        X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
nic-hdl:        ROLE-5
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

% Information related to '192.0.2.0/25AS00001'

route:          192.0.2.0/2
descr:          X
origin:         AS00001
org:            ORG-3
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE

organisation:   ORG-3
org-name:       X
org-type:       X
address:        X
address:        X
address:        X
address:        X
phone:          X
fax-no:         X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
admin-c:        X
abuse-c:        ROLE-5
mnt-ref:        X
mnt-ref:        X
mnt-by:         X
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

% This query was served by the RIPE Database Query Service version 1.98 (BLAARKOP)


OUTPUT
        '2001:db8::1' => <<'OUTPUT',
% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to '2001:db8::/32'

% Abuse contact for '2001:db8::/32' is 'bad@example.com'

inet6num:       2001:db8::/32
netname:        X
org:            ORG-4
country:        X
admin-c:        PERSON-5
tech-c:         ROLE-7
status:         X
mnt-by:         X
mnt-by:         X
mnt-routes:     X
mnt-domains:    X
created:        X
last-modified:  X
source:         RIPE

organisation:   ORG-4
org-name:       X
org-type:       X
descr:          X
address:        X
address:        X
address:        X
address:        X
phone:          X
fax-no:         X
admin-c:        X
admin-c:        X
abuse-c:        ROLE-4
mnt-ref:        X
mnt-ref:        X
mnt-by:         X
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

role:           X
address:        X
address:        X
address:        X
phone:          X
fax-no:         X
abuse-mailbox:  bad@example.org
admin-c:        PERSON-5
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
tech-c:         X
nic-hdl:        ROLE-7
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

person:         X
address:        X
address:        X
address:        X
address:        X
address:        X
phone:          X
fax-no:         X
nic-hdl:        PERSON-5
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

% Information related to '2001:db8::/32AS0001'

route6:         2001:db8::/32
descr:          X
origin:         AS0001
mnt-lower:      X
mnt-routes:     X
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE

% This query was served by the RIPE Database Query Service version 1.98 (ANGUS)


OUTPUT
        'ROLE-4' => <<'OUTPUT',
% This is the RIPE Database query service.
% The objects are in RPSL format.
%
% The RIPE Database is subject to Terms and Conditions.
% See http://www.ripe.net/db/support/db-terms-conditions.pdf

% Note: this output has been filtered.
%       To receive output for a database update, use the "-B" flag.

% Information related to 'ROLE-4'

role:           X
remarks:        X
address:        X
address:        X
address:        X
address:        X
abuse-mailbox:  good@example.com
nic-hdl:        ROLE-4
mnt-by:         X
created:        X
last-modified:  X
source:         RIPE # Filtered

% This query was served by the RIPE Database Query Service version 1.98 (WAGYU)


OUTPUT
    },
};

our $server = undef;

no warnings qw(once redefine);

*App::Abuser::address2rir = sub {
    my ($ip) = @_;
    if (!defined $ip) {
        die "Missing an argument for App::Abuser::address2rir().\n";
    }
    if (!defined $server) {
        die "You forgot to override WHOIS server with setting a \$Stub::server variable.\n";
    }
    return $server;
};

*App::Abuser::query = sub {
    my ($self, @arguments) = @_;

    my $key = $examples;
    for my $arg (@arguments) {
        if (ref $key eq '') {
            die "Too many arguments: App::Abuser::query(" . join(', ', @arguments) . ")\n"; 
        }
        if (!exists $key->{$arg}) {
            die "Unknown argument '$arg' in: App::Abuser::query(" . join(', ', @arguments) . ")\n";
        }
        $key = $key->{$arg};
    }

    if (ref $key ne '') {
        die "Too few arguments: App::Abuser::query(" . join(', ', @arguments) . ")\n";
    }

    return $key;
};

1;
