use Test::More tests => 4;
use App::Abuser ();
require './t/Stub.pm';
$Stub::server = 'whois.apnic.net';

is(App::Abuser->new->search('192.0.2.0'), 'good@example.com', 'inetnum with abuse-c');
is(App::Abuser->new->search('192.0.2.4'), 'good@example.com', 'inetnum with case non-matching tech-c');
is(App::Abuser->new->search('192.0.2.8'), 'good@example.com', 'STUB-delegation from APNIC to RIPE');
is(App::Abuser->new->search('192.0.2.9'), 'good@example.com', 'role with multiple abuse-mailboxes');
