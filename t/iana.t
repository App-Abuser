use Test::More tests => 1;
use App::Abuser ();
require './t/Stub.pm';
$Stub::server = 'whois.iana.org';

is(App::Abuser->new->search('0.0.0.0'), undef, '0.0.0.0 is reserved by IANA');
