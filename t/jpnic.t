use Test::More tests => 2;
use App::Abuser ();
require './t/Stub.pm';
$Stub::server = 'whois.apnic.net';

is(App::Abuser->new->search('192.0.2.7'), 'good@example.com', 'APNIC to JPNIC to "Contact Information"');
is(App::Abuser->new()->search('2001:db8::2'), 'good@example.com', 'inet6num APNIC to JPNIC to "Contact Information"');
