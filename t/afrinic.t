use Test::More tests => 1;
use App::Abuser ();
require './t/Stub.pm';
$Stub::server = 'whois.afrinic.net';

is(App::Abuser->new->search('192.0.2.3'), 'good@example.com', 'afrinic to apnic to jpnic to a handle with an e-mail');
