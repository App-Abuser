use Test::More tests => 1;
use App::Abuser ();
require './t/Stub.pm';
$Stub::server = 'whois.lacnic.net';

is(App::Abuser->new->search('192.0.2.6'), 'good@example.com', 'inetnum with abuse-c with e-mail');
